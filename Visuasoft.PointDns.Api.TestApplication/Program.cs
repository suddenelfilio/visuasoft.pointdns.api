﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Visuasoft.PointDns.Api.TestApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            string username = "filip-pointdns@suddenelfilio.net";
            string pwd = "ae6a0512-26b1-e2c7-3a74-d5c9369412b6";
            var pdns = new PointDNS(new System.Net.NetworkCredential(username, pwd));

            var zones = pdns.GetAsync().Result;
            Console.WriteLine("{0} zones found.", zones.Count());

            if (!zones.Any(z => z.Name == "testapie.be"))
            {
                var zone = new Zone
                {
                    Name = "testapie.be",
                    Group = "apietest"
                };

                zone = pdns.SaveAsync(zone).Result;
                Console.WriteLine("Zone {0} was created with ID {1}", zone.Name, zone.ID);
                zone.Group = "apitest";
                zone = pdns.SaveAsync(zone).Result;
                Console.WriteLine("Zone {0} was created with ID {1} in group {2}", zone.Name, zone.ID, zone.Group);

                var records = pdns.GetRecordsAsync(zone).Result;
                Console.WriteLine("There are {0} records in zone {1}", records.Count(), zone.Name);

                zones = pdns.GetAsync().Result;
                Console.WriteLine("{0} zones found.", zones.Count());
            }

            var myZone = zones.FirstOrDefault(z => z.Name == "testapie.be");
            if (myZone != null)
            {
                var records = pdns.GetRecordsAsync(myZone).Result;
                foreach (var record in records)
                {
                    Console.WriteLine("record {0} [{3}]: data= {1} ttl: {2}", record.Name, record.Data, record.TTL, record.RecordType);
                }

                var cnameRecord = new ZoneRecord
                {
                    Name = "test.testapie.be",
                    RecordType = "CNAME",
                    TTL = 300,
                    Data = "www.google.be",
                    ZoneID = myZone.ID
                };

                cnameRecord = pdns.SaveAsync(cnameRecord).Result;

                Console.WriteLine("{0} record {1} created with id {2}", cnameRecord.RecordType, cnameRecord.Name, cnameRecord.ID);

                cnameRecord.Name = "test";
                cnameRecord = pdns.SaveAsync(cnameRecord).Result;
                Console.WriteLine("{0} record {1} updated with id {2}", cnameRecord.RecordType, cnameRecord.Name, cnameRecord.ID);

                Console.WriteLine("record deleted:{0}", pdns.DeleteAsync(cnameRecord).Result);
            }
            Console.WriteLine("zone deleted:{0}", pdns.DeleteAsync(myZone).Result);
            Console.ReadLine();
        }
    }
}
