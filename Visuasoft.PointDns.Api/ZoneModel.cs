﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Visuasoft.PointDns.Api
{
    internal class ZoneModel
    {
        [JsonProperty("zone")]
        public Zone Zone { get; set; }

        public ZoneModel()
        {

        }

        public ZoneModel(Zone zone)
        {
            this.Zone = zone;
        }
    }
}
