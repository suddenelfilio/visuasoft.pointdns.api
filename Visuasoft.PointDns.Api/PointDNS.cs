﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Visuasoft.PointDns.Api
{
    public class PointDNS
    {
        private HttpClient client = new HttpClient();

        public NetworkCredential ApiCredentials { get; set; }

        public PointDNS(NetworkCredential credentials)
        {
            if (credentials == null)
                throw new ArgumentNullException("credentials");

            this.ApiCredentials = credentials;
            string authbase = string.Concat(credentials.UserName, ":", credentials.Password);
            this.client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("basic", Convert.ToBase64String(Encoding.ASCII.GetBytes(authbase)));
            this.client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            this.client.DefaultRequestHeaders.UserAgent.Add(new ProductInfoHeaderValue("PointDnsdotNet","1.0"));
        }

        public PointDNS(string username, SecureString password) :
            this(new NetworkCredential(username, password))
        {
        }

        #region Zone Functions
        public async Task<Zone> SaveAsync(Zone zone)
        {
            if (zone == null)
                throw new ArgumentNullException("zone");

            if (this.ApiCredentials == null)
                throw new InvalidProgramException("You must provide your API Credentials before performing any operations.");

            if (zone.ID <= 0)
                zone = await this.CreateZoneAsync(zone);
            else
                zone = await this.UpdateZoneAsync(zone);

            return zone;
        }

        public async Task<IEnumerable<Zone>> GetAsync()
        {
            var response = await this.client.GetAsync("https://pointhq.com/zones");
            if (response.StatusCode == HttpStatusCode.OK)
            {
                string responseJson = await response.Content.ReadAsStringAsync();
                return (await JsonConvert.DeserializeObjectAsync<IEnumerable<ZoneModel>>(responseJson)).Select(z => z.Zone).ToList();
            }
            return null;
        }

        public async Task<Zone> GetAsync(Zone zone)
        {
            if (zone == null)
                throw new ArgumentNullException("zone");

            if (zone.ID <= 0)
                throw new InvalidOperationException("Cannot get zone when the id is <= zero.");

            return await this.GetZoneAsync(zone.ID);
        }

        public async Task<Zone> GetZoneAsync(int zoneID)
        {
            if (zoneID <= 0)
                throw new InvalidOperationException("Cannot get zone when the id is <= zero.");

            var response = await this.client.GetAsync(string.Concat("https://pointhq.com/zones/", zoneID));
            if (response.StatusCode == HttpStatusCode.OK)
            {
                string responseJson = await response.Content.ReadAsStringAsync();
                return (await JsonConvert.DeserializeObjectAsync<ZoneModel>(responseJson)).Zone;
            }

            return null;
        }

        public async Task<bool> DeleteAsync(Zone zone)
        {
            if (zone == null)
                throw new ArgumentNullException("zone");

            if (zone.ID <= 0)
                throw new InvalidOperationException("Cannot delete zone when the id is <= zero.");

            return await this.DeleteZoneAsync(zone.ID);
        }

        public async Task<bool> DeleteZoneAsync(int zoneID)
        {
            if (zoneID <= 0)
                throw new InvalidOperationException("Cannot delete zone when the id is <= zero.");

            var response = await this.client.DeleteAsync(string.Concat("https://pointhq.com/zones/", zoneID));
            string responseJson = await response.Content.ReadAsStringAsync();
            return (await JsonConvert.DeserializeObjectAsync<dynamic>(responseJson)).zone.status == "OK";
        }

        private async Task<Zone> CreateZoneAsync(Zone zone)
        {
            string json = await JsonConvert.SerializeObjectAsync(new ZoneModel(zone));
            var content = new StringContent(json, ASCIIEncoding.UTF8, "application/json");
            content.Headers.ContentType = new MediaTypeWithQualityHeaderValue("application/json");
            HttpResponseMessage response = await this.client.PostAsync("https://pointhq.com/zones", content);
            if (response.StatusCode == HttpStatusCode.Created)
            {
                string responseJson = await response.Content.ReadAsStringAsync();
                return (await JsonConvert.DeserializeObjectAsync<ZoneModel>(responseJson)).Zone;
            }
            
            return null;
        }

        private async Task<Zone> UpdateZoneAsync(Zone zone)
        {
            string json = await JsonConvert.SerializeObjectAsync(new ZoneModel(zone));
            var content = new StringContent(json, ASCIIEncoding.UTF8, "application/json");
            content.Headers.ContentType = new MediaTypeWithQualityHeaderValue("application/json");

            var response = await this.client.PutAsync(string.Concat("https://pointhq.com/zones/", zone.ID), content);
            if (response.StatusCode == HttpStatusCode.Accepted)
            {
                string responseJson = await response.Content.ReadAsStringAsync();
                return (await JsonConvert.DeserializeObjectAsync<ZoneModel>(responseJson)).Zone;
            }
            return null;
        }
        #endregion

        #region Zone Record functions

        public async Task<ZoneRecord> SaveAsync(ZoneRecord record)
        {
            if (record == null)
                throw new ArgumentNullException("record");

            if (record.ID <= 0)
                record = await this.CreateZoneRecordAsync(record);
            else
                record = await this.UpdateZoneRecordAsync(record);

            return record;
        }

        public async Task<bool> DeleteAsync(ZoneRecord record)
        {
            if (record == null)
                throw new ArgumentNullException("record");

            if (record.ID <= 0)
                throw new InvalidOperationException("Cannot delete record when the id is <= zero.");

            if (record.ZoneID <= 0)
                throw new InvalidOperationException("Cannot delete record when zone id is <= zero.");

            return await this.DeleteZoneRecordAsync(record.ZoneID, record.ID);
        }

        public async Task<bool> DeleteZoneRecordAsync(int zoneID, int recordID)
        {
            if (zoneID <= 0)
                throw new InvalidOperationException("Cannot delete zone when the id is <= zero.");

            if (recordID <= 0)
                throw new ArgumentNullException("recordID");

            var response = await this.client.DeleteAsync(string.Format("https://pointhq.com/zones/{0}/records/{1}", zoneID, recordID));
            if (response.StatusCode == HttpStatusCode.Accepted)
            {
                string responseJson = await response.Content.ReadAsStringAsync();
                return (await JsonConvert.DeserializeObjectAsync<dynamic>(responseJson)).zone_record.status == "OK";
            }

            return false;
        }

        public async Task<IEnumerable<ZoneRecord>> GetRecordsAsync(Zone zone)
        {
            if (zone == null)
                throw new ArgumentNullException("zone");

            if (zone.ID <= 0)
                throw new InvalidOperationException("Cannot get the records for a zone with ID <= zero");
            
            return await this.GetRecordsAsync(zone.ID);
        }

        public async Task<IEnumerable<ZoneRecord>> GetRecordsAsync(int zoneID)
        {
            if (zoneID <= 0) throw new InvalidOperationException("Cannot get the records for a zone with id <= zero");

            var response = await this.client.GetAsync(string.Format("https://pointhq.com/zones/{0}/records", zoneID));
            if (response.StatusCode == HttpStatusCode.OK)
            {
                string responseJson = await response.Content.ReadAsStringAsync();
                return (await JsonConvert.DeserializeObjectAsync<IEnumerable<ZoneRecordModel>>(responseJson)).Select(zr => zr.Record).ToList();
            }

            return null;
        }

        public async Task<ZoneRecord> GetRecordAsync(Zone zone, int recordID)
        {
            if (zone == null)
                throw new ArgumentNullException("zone");

            if (zone.ID <= 0)
                throw new InvalidOperationException("Cannot get the record for zone with ID <= 0");

            if (recordID <= 0)
                throw new ArgumentNullException("recordID");

            return await this.GetRecordAsync(zone.ID, recordID);
        }

        public async Task<ZoneRecord> GetRecordAsync(int zoneID,int recordID)
        {
            if (recordID <= 0)
                throw new ArgumentNullException("recordID");

            if (zoneID <= 0)
                throw new ArgumentNullException("zoneID");

            var response = await this.client.GetAsync(string.Format("https://pointhq.com/zones/{0}/records/{1}", zoneID, recordID));
            if (response.StatusCode == HttpStatusCode.OK)
            {
                string responseJson = await response.Content.ReadAsStringAsync();
                return (await JsonConvert.DeserializeObjectAsync<ZoneRecordModel>(responseJson)).Record;
            }

            return null;
        }

        private async Task<ZoneRecord> CreateZoneRecordAsync(ZoneRecord record)
        {
            if (record == null)
                throw new ArgumentNullException("record");

            if (record.ZoneID <= 0)
                throw new InvalidOperationException("Cannot update a record the zone Id <= zero");

            string json = await JsonConvert.SerializeObjectAsync(new ZoneRecordModel(record));
            var content = new StringContent(json, ASCIIEncoding.UTF8, "application/json");
            content.Headers.ContentType = new MediaTypeWithQualityHeaderValue("application/json");            
            HttpResponseMessage response = await this.client.PostAsync(string.Format("https://pointhq.com/zones/{0}/records", record.ZoneID), content);
            if (response.StatusCode == HttpStatusCode.Created)
            {
                string responseJson = await response.Content.ReadAsStringAsync();
                return (await JsonConvert.DeserializeObjectAsync<ZoneRecordModel>(responseJson)).Record;
            }

            return record;
        }

        private async Task<ZoneRecord> UpdateZoneRecordAsync(ZoneRecord record)
        {
            if (record == null)
                throw new ArgumentNullException("record");

            if (record.ID <= 0)
                throw new InvalidOperationException("Cannot update a record when the ID is <= zero");

            if (record.ZoneID <= 0)
                throw new InvalidOperationException("Cannot update a record the zone Id <= zero");

            string json = await JsonConvert.SerializeObjectAsync(new ZoneRecordModel(record));
            var content = new StringContent(json, ASCIIEncoding.UTF8, "application/json");
            content.Headers.ContentType = new MediaTypeWithQualityHeaderValue("application/json");
           
            HttpResponseMessage response = await this.client.PutAsync(string.Format("https://pointhq.com/zones/{0}/records/{1}", record.ZoneID,record.ID), content);
            if (response.StatusCode == HttpStatusCode.Accepted)
            {
                string responseJson = await response.Content.ReadAsStringAsync();
                return (await JsonConvert.DeserializeObjectAsync<ZoneRecordModel>(responseJson)).Record;
            }

            return record;
        }
        #endregion

    }
}
