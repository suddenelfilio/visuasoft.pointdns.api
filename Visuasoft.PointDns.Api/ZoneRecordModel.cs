﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Visuasoft.PointDns.Api
{
    internal class ZoneRecordModel
    {
        [JsonProperty("zone_record")]
        public ZoneRecord Record { get; set; }

        public ZoneRecordModel()
        {
        }

        public ZoneRecordModel(ZoneRecord record)
        {
            this.Record = record;
        }
    }
}
