﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace Visuasoft.PointDns.Api
{
    public class Zone
    {
        [JsonProperty("id")]
        public int ID { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("ttl")]
        public int TTL { get; set; }

        [JsonProperty("group")]
        public string Group { get; set; }

        [JsonProperty("user-id")]
        public int UserID { get; set; }    
    }
}
