﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace Visuasoft.PointDns.Api
{
    public class ZoneRecord
    {
        [JsonProperty("id")]
        public int ID { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("data")]
        public string Data { get; set; }
        
        [JsonProperty("ttl")]
        public int TTL { get; set; }

        [JsonProperty("aux")]
        public string Aux { get; set; }

        [JsonProperty("zone_id")]
        public int ZoneID { get; set; }

        [JsonProperty("record_type")]
        public string RecordType { get; set; }

        [JsonProperty("redirect_to")]
        public string RedirectTo { get; set; }
    }
}
